%global debug_package %{nil}
%global _efi_vendor_ %(eval sed -n -e 's/^ID=//p' /etc/os-release)
%global _efi_vendor_dir %(eval sed -n -e 's/^ID=//p' /etc/os-release | awk -F '"' '{print $2}')

Name:          efi-rpm-macros
Version:       4
Release:       10
Summary:       A set of EFI-related RPM Macros
License:       GPLv3+
URL:           https://github.com/rhboot/%{name}/
Source0:       https://github.com/rhboot/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

%ifarch  loongarch64
Patch0:        add-loongarch64-support-for-efi-arch.patch
%endif
%ifarch sw_64
Patch1:        efi-rpm-macros-Add-sw64-architecture.patch
%endif
Patch2:        0001-add-riscv64-support-for-efi-arch.patch

BuildRequires: git sed
BuildArch:     noarch

%description
Efi-rpm-macros help to build EFI-Related packages with a set of common RPM Macros.

%package -n efi-srpm-macros
Summary: A set of EFI-related SRPM Macros
BuildArch: noarch
Requires: rpm

%description -n efi-srpm-macros
Efi-srpm-macros help to build EFI-related packages with a set of common SRPM macros.

%package -n efi-filesystem
Summary: Basic diretory layout of EFI machines
BuildArch: noarch
Requires: filesystem

%description -n efi-filesystem
Efi-filesystem is the structure and logic rules used to manage the groups of
information of EIF machines, it contains the basic directory layout for EFI
machine bootloaders and tools.

%prep
%autosetup -S git -n %{name}-4
git config --local --add efi.vendor "%{_vendor}"
git config --local --add efi.esp-root /boot/efi
git config --local --add efi.arches "x86_64 aarch64 loongarch64 sw_64 riscv64"

%build
%make_build EFI_VENDOR=%{_efi_vendor_} clean all

%install
%make_install EFI_VENDOR=%{_efi_vendor_}

%files -n efi-srpm-macros
%license LICENSE
%doc README
%{_rpmmacrodir}/macros.efi-srpm

%files -n efi-filesystem
%defattr(0700,root,root,-)
%dir /boot/efi/EFI/BOOT
%dir /boot/efi/EFI/%{_efi_vendor_dir}
%{_prefix}/lib/rpm/brp-boot-efi-times

%changelog
* Wed May 29 2024 Ouuleilei <wangliu@iscas.ac.cn> - 4-10
- Add riscv64 architecture

* Tue Nov 29 2022 wuzx<wuzx1226@qq.com> - 4-9
- Add sw64 architecture

* Thu Nov 24 2022 yanglu <yanglu72@h-partners.com> - 4-8
- DESC:add loongarch64 support for efi_arch

* Sat Nov 19 2022 Wenlong Zhang <zhangwenlong@loongson.cn> - 4-7
- add loongarch support for efi-rpm-macros

* Thu Jun 16 2022 yanglu <yanglu72@h-partners.com> - 4-6
- DESC:delete macros in changelog

* Tue Aug 17 2021 gaihuiying <gaihuiying1@huawei.com> - 4-5
- DESC:revert "delete -S git from autosetup, and delete BuildRequires git"

* Tue Aug 03 2021 chenyanpanHW <chenyanpan@huawei.com> - 4-4
- DESC: delete -S git from autosetup, and delete BuildRequires git

* Thu Sep 10 2020 hanzhijun <hanzhijun1@huawei.com> - 4-3
- solve source url problem 

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 4-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:bugfix in build process

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 4-1
- Package update. 

* Tue Sep 17 2019 yanzhihua <yanzhihua4@huawei.com> - 3-4
- Package init.

